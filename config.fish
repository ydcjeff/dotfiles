set -gx EDITOR code
set -gx DOTFILES_PATH $HOME/.dotfiles
set -gx XDG_CONFIG_HOME $HOME/.config
set -gx XDG_CACHE_HOME $HOME/.cache
set -gx STARSHIP_CONFIG $DOTFILES_PATH/starship.toml
set -gx FISH_CONFIG $XDG_CONFIG_HOME/fish/config.fish
set -gx TERM xterm-256color
set -gx fish_color_command 00ff00
set -gx fish_greeting

fish_add_path $HOME/.cargo/bin
fish_add_path $HOME/.deno/bin
fish_add_path $HOME/.composer/vendor/bin
fish_add_path $HOME/.local/share/pnpm

source $DOTFILES_PATH/abbr.fish

# mac or linux
switch (uname)
	case Linux
		echo "> Linux profile $FISH_CONFIG loaded."
	case Darwin
		echo "> macOS profile $FISH_CONFIG loaded."
end

starship init fish | source
