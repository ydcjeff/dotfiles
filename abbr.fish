#!/usr/bin/env fish

# directory navigation
abbr -a ..    "cd .."
abbr -a ...   "cd ../.."
abbr -a ....  "cd ../../.."
abbr -a ..... "cd ../../../.."

# some shortcuts
abbr -a c   "clear"
abbr -a rem "rm -rf"

# npm shortcuts
abbr -a p "pnpm"

# python shortcuts
abbr -a py "python3"
abbr -a pt "pytest"

# rust shortcuts
abbr -a co "cargo"

# deno shortcuts
abbr -a d "deno"

# git shortcuts
abbr -a gs  "git status"
abbr -a ga  "git add"
abbr -a gc  "git commit"
abbr -a gb  "git branch"
abbr -a gd  "git diff"
abbr -a gcl "git clone"
abbr -a gco "git checkout"
abbr -a gp  "git push"
abbr -a gl  "git pull"
abbr -a gt  "git tag"
abbr -a gm  "git merge"
abbr -a gf  "git fetch"
abbr -a gr  "git rebase"
abbr -a gg  "git log --graph --pretty=format:\"%C(bold red)%h%Creset -%C(bold yellow)%d%Creset %s %C(bold green)(%cr) %C(bold blue)<%an>%Creset %C(yellow)%ad%Creset\" --abbrev-commit --date=short"
abbr -a ggr "git log --reverse --pretty=format:\"%C(bold red)%h%Creset -%C(bold yellow)%d%Creset %s %C(bold green)(%cr) %C(bold blue)<%an>%Creset %C(yellow)%ad%Creset\" --abbrev-commit --date=short"

# dotfiles
abbr -a dot "cd ~/.dotfiles"
abbr -a rl  "source $FISH_CONFIG"

# recursively delete .DS_Store
abbr -a dds "find . -name \"*.DS_Store\" -type f -ls -delete"

# recursively delete node_modules
abbr -a nodemodules "find . -type d -name \"node_modules\" -exec rm -rf \"{}\" +"
