# .files

Includes:

- Fishshell settings
- [Starship shell theme](https://starship.rs)

Requires:

- git
- fish
- curl

Setup:

```sh
curl -fsSL https://gitlab.com/ydcjeff/dotfiles/-/raw/main/setup.sh | sh
```
