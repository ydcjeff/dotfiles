#!/usr/bin/env sh

set -eo pipefail

has() {
	command -v "$1" >/dev/null 2>&1
}

# clone dotfiles
if [ -d $HOME/.dotfiles ]; then
	echo "> .dotfiles dir exist, not cloning"
else
	git clone https://gitlab.com/ydcjeff/dotfiles $HOME/.dotfiles
fi

# install starship
if has starship; then
	echo "> $(starship -V) exist, not installing"
else
	curl -sS https://starship.rs/install.sh | sh
fi

# source dot files
echo "> Sourcing dotfiles"
mkdir -p $HOME/.config/fish
ln -sf $HOME/.dotfiles/config.fish $HOME/.config/fish/config.fish

# git defaults
git config --global color.ui true
git config --global push.default simple
git config --global fetch.prune true
git config --global pull.rebase true
git config --global user.name "ydcjeff"
git config --global user.email "n2ya@duck.com"
git config --global core.editor "nvim"
git config --global init.defaultBranch main
git config --global includeIf.gitdir/i:$HOME/csit/.path $HOME/.dotfiles/gitconfig-csit
